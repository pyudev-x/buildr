use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct ProjectInfo {
    pub name:String,
    pub compiler:String
}

impl Default for ProjectInfo {
    fn default() -> Self {
        return ProjectInfo { name: "example".to_string(), compiler: "gcc".to_string() }
    }
}
