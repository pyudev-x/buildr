use std::{process::Command, path::Path, io::{prelude::*, BufReader}, fs::{read_to_string, File}};
mod project; use project::*;

const BASIC_PROGRAM:&str = 
"#include <stdio.h>
int main(void) {
\tprintf(\"hello world\\n\");
\treturn 0;
}";


fn main() {
    loop {
        let action = read("buildr > ");

        match action.as_str() {
            "init" => {
                use std::fs::create_dir;
                
                let name = read("Project name: ");
                let compiler = read("Compiler for project: ");

                if create_dir("src").is_ok() && create_dir("lib").is_ok() && create_dir("out").is_ok() {
                    File::create("src/main.c").unwrap().write_all(BASIC_PROGRAM.as_bytes()).unwrap();
                    File::create("build.br").unwrap();

                    let json = format!("{{
                                            \"name\":\"{name}\",
                                            \"compiler\":\"{compiler}\"
                                        }}");


                    File::create("project.json").unwrap().write_all(json.as_bytes()).unwrap();
                    println!("Project created");
                } else {
                    println!("Failed to create project");
                }
            },
            "build" => {
                build_project(Path::new("build.txt"));
            },
            "shell" => { // quickly open a shell running SH if needed
                Command::new("sh").spawn().unwrap().wait().unwrap(); 
            },
            "exit" => break,
            _ => {}
        }
    }
}

fn build_project(build_file:&Path) {
    if build_file.exists() {
        let file = read_to_string(build_file).unwrap();

        let reader_file = File::open("project.json").unwrap(); let reader = BufReader::new(reader_file);
        let project_info: ProjectInfo = serde_json::from_reader(reader).unwrap();
        
        let mut compile_args = format!("{} src/**.c -I lib/ -o out/{}", project_info.compiler, project_info.name);
        
        parse_build_script(file, &mut compile_args);

        // Build the project
        Command::new("sh").args(vec!["-c", compile_args.as_str()])
            .spawn().unwrap()
            .wait().unwrap();

        // Run the project
        Command::new(format!("./out/{}", project_info.name))
            .spawn().unwrap()
            .wait().unwrap();
    } else {
        println!("No build file!");
    }
}

fn parse_build_script(file_contents:String, compile_args:&mut String) {
    let script_lines: Vec<&str> = file_contents.split("\n").collect::<Vec<&str>>();

    macro_rules! add_arg {
        ($arg:expr) => {
            compile_args.push_str(format!(" {}", $arg).as_str());
        };
    }

    for line in script_lines {
        if line.is_empty() {continue;}

        let split: Vec<&str> = line.split(" ; ").collect::<Vec<&str>>();

        let cmd = split[0];
        let arg = split[1];

        match cmd {
            "add_include_path" => {
                add_arg!(format!("-I {arg}"));
            },

            "add_library_path" => {
                add_arg!(format!("-L {arg}"));
            },

            "add_library" => {
                add_arg!(format!("-l{arg}"));
            },

            "add_arg" => {
                add_arg!(arg);
            }

            _ => {}
        }
    } 
}

fn read(input:&str) -> String {
    use std::io::stdin;
    let mut value = String::new();
    eprint!("{}", input);
    stdin().read_line(&mut value).unwrap();
    return value.trim().to_string();
}
