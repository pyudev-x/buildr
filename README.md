# buildr

Basic build system for the C programming language

## Installation

Simply run `cargo install --git https://gitlab.com/pyudev-x/buildr`

## Usage

Run `buildr` in your terminal. You should see a prompt with the input `buildr> `

A list of commands are:

- init (Create a project)
- build (Build and run your project)
- shell (Run `sh` in the prompt)
- exit (Exit the prompt)
